#Team problem codeforces
#https://codeforces.com/problemset/problem/231/A

n = int(input())

problems = []
for i in range(n):
    problems.append(input())


count_rows = 0
for i in problems:
    count_one = 0
    for j in i.split():
        if int(j) == 1:
            count_one+=1
    if count_one >= 2:
        count_rows+=1

print(count_rows)
    




#Cold-puter science problem kattis
#https://open.kattis.com/problems/cold
n = int(input())

temperatures = [int(i) for i in input().split()]

count= 0

for temp in temperatures:
    if temp < 0:
        count += 1

print (count)

#Domino piling problem codeforces
#https://codeforces.com/problemset/problem/50/A
nums = [int(i) for i in input().split()]

print((nums[0]*nums[1])//2)

#kemija solution using a while loop
#https://open.kattis.com/problems/kemija08
code = input()

sentence = ""

counter = 0


vowels = ["a", "e", "i", "o", "u"]


while counter < len(code):
    sentence += code[counter]
    if code[counter] in vowels:
        counter += 2
    counter +=1

print(sentence)

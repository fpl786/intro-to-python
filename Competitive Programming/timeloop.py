#timeloop problem kattis
#https://open.kattis.com/problems/timeloop
n = int(input())
for i in range(n):
    print(str(i+1) + " Abracadabra")

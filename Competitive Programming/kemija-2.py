#kemija problem using .replace() function
#https://open.kattis.com/problems/kemija
vowels = ["a", "e", "i", "o", "u"]
word = input()
for i in vowels:
    word = word.replace(i+"p"+i,i)
print(word)

#Missing numbers problem kattis
#https://open.kattis.com/problems/missingnumbers
n = int(input())

numbers = []

for i in range(n):
    numbers.append(int(input()))


end = numbers[-1]

correct = True
for i in range(1, end):
    if i not in numbers:
        correct = False
        print(i)


if correct:
    print("good job")

'''
write a program that takes a word as input
and prints only the first 3 letters

'''

print("Type a word: ")

word = input()

print("First three letters: "+ word[:3])

'''
write a program that takes a the number of minutes as input
and converts it to seconds and prints it to the screen.

Eg.
Enter the number of minutes
2
120 seconds

'''
print("Enter the number of minutes")

minutes = int(input())

print(str(minutes*60) + " seconds")

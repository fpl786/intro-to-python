'''

Build off of the previous program and give the user 3 guesses
if all 3 guesses are incorrect, print the correct answer at the end
of the 3 guesses

if the user is correct, print correct and exit the program

Eg. 1

Guess the number: 5
Try Again
Guess the number: 10
Try Again
Guess the number: 3
Incorrect! The answer is 7


Eg. 2

Guess the number: 9
Try Again
Guess the number: 4
Correct!

Eg. 3

Guess the number: 12
Correct!


'''
import random

number = random.randint(1, 20)
print(number)

guess = int(input("Guess the number: "))

if guess != number:
    print("Try Again")
    guess = int(input("Guess the number: "))
    if guess != number:
        print("Try Again")
        guess = int(input("Guess the number: "))
        if guess != number:
            print("Incorrect! The correct answer is", number)
        else:
            print("Correct")
    else:
        print("Correct")
else:
    print("Correct")

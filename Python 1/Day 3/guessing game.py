'''
using while loops create a guessing game

if the guess is too high print "Too High"
if the guess is too low print "Too Low"
if the guess is correct print "Correct" and
break out of the loop

Give user 5 guesses

MUST USE A WHILE LOOP

'''
import random

count = 0

r = random.randint(1, 50)

print(r)
while count < 5:
    guess = int(input("Guess the number: "))

    if guess > r:
        print("Too High")
    elif guess < r:
        print("Too Low")
    else:
        print("Correct")
        break
    count += 1


if guess != r:
    print("Incorrect! The correct number is", r)
    

'''

Build a simple guessing game
generate a random number from 1 to 20
Ask the user to guess the number.

If the guess is correct, print "You Win"
otherwise print "You lose, the correct is ___"

'''
import random

number = random.randint(1, 20)


guess = int(input("Guess the number: "))


if number == guess:
    print("You Win")
else:
    print("You Lose, the correct answer is", number)
    

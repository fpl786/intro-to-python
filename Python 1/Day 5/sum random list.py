'''

Generate a list of 10 random numbers from 1 to 50.
Sum up the list of numbers.

HINT: Use the .append() function in a for loop to append a random
number to the list 

'''
import random

numbers = []


for i in range(10):
    numbers.append(random.randint(1, 50))

add = 0

for i in numbers:
    add += i



print(numbers)
print("Sum of List: ",  add)

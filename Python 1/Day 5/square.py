'''
Write a turtle program to draw a square using a for loop

'''

import turtle


t = turtle.Turtle()


t.begin_fill()
for i in range(4):
    t.forward(100)
    t.right(90)

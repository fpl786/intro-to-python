'''
Write a turtle program to draw a square using a for loop

'''

import turtle


t = turtle.Turtle()


t.shape("turtle")


t.pensize(5)
t.pencolor("blue")
t.fillcolor("green")

t.begin_fill()
for i in range(4):
    t.forward(100)
    t.right(90)
t.end_fill()


t.fillcolor("chartreuse")
t.penup()
t.backward(200)
t.pendown()

t.begin_fill()
t.circle(40)
t.end_fill()


t.penup()

t.forward(100)

t.pendown()

t.dot(30)

'''

6! = 6 x 5 x 4 x 3 x 2 x 1 = 720

3! = 3 x 2 x 1 = 6

2! = 2 x 1 = 2


Write a program that takes a number as input
and calculates the factorial of the number

You will need to use a for loop

'''

n = int(input("Type a number: "))

factorial = 1

for i in range(1, n + 1):
    factorial *= i
print("Factorial of the number is " + str(factorial))

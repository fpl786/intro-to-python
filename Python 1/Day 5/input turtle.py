'''

Take in a pen color and fill color as input
draw a square with those colors

Take in another pen color and fill color as input
draw a circle with those colors. Draw the circle in a different location

'''
import turtle


t = turtle.Turtle()

penSquare = input("Enter pen color of square: ")
fillSquare = input("Enter fill color of square: ")
penCircle = input("Enter pen color of circle: ")
fillCircle = input("Enter fill color of circle: ")
t.pensize(10)
t.fillcolor(fillSquare)
t.pencolor(penSquare)

t.begin_fill()

for i in range(4):
    t.forward(100)
    t.right(90)
    
t.end_fill()

t.penup()
t.backward(200)
t.pendown()


t.fillcolor(fillCircle)
t.pencolor(penCircle)

t.begin_fill()

t.circle(40)

t.end_fill()

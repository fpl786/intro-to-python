'''
write a program that sums up the following list

l = [5,6,7,32,4,5, 76,1,3]

'''

l = [5,6,7,32,4,5, 76,1,3]

s = 0

for i in l:
    s += i
    
print ("Sum: ", s)

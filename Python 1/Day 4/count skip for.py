'''
print all numbers from 1 to 20
except for 1, 5 , 9 , 12, and 16
You MUST use a for loop
'''
for count in range(1,21):
    if count != 1 and count != 5 and count!= 9 and count!= 12 and count!= 16:
        print(count)


for count in range(1,21):
    if count == 1 or  count == 5 or count== 9 or count== 12 or count== 16:
        continue
        
    print(count)

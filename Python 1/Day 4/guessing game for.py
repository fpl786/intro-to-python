'''
using for loops create a guessing game

if the guess is too high print "Too High"
if the guess is too low print "Too Low"
if the guess is correct print "Correct" and
break out of the loop

Give user 5 guesses

MUST USE A FOR LOOP

'''
import random



r = random.randint(1, 50)


for i in range(5):
    guess = int(input("Guess the number: "))

    if guess > r:
        print("Too High")
    elif guess < r:
        print("Too Low")
    else:
        print("Correct")
        break
    


if guess != r:
    print("Incorrect! The correct number is", r)
    

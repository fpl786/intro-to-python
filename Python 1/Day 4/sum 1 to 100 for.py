'''
Using a for loop sum all the numbers from 1 to 100

'''
s = 0

for i in range(1,101):
    s += i
    
print(s)

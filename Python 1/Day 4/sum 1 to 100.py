'''
Write a program that sums up all the numbers from 1 to 100
using a while loop

Answer: 5050

'''


counter = 1

s = 0

while counter <= 100:
    s += counter
    counter +=1

print("Sum: ", s)

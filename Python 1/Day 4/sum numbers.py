'''
Write a program that adds up all the numbers the user enters.

Eg. 
Enter the amount of numbers to sum: 5
5
6
23
2
1
Sum: 37

'''


n = int(input("Enter the amount of numbers to sum: "))

counter = 0
s = 0

while counter < n:
    s += int(input())
    counter += 1

print("Sum:", s)


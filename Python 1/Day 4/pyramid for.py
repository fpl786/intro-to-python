'''
Ask the user to enter a number
print a pyramid like below with that
number of rows

Hint: You need a for loop
      Use the string replication (*) operator for strings

Eg. 1

Enter a number: 5

*
**
***
****
*****

Eg. 2

Enter a number: 7

*
**
***
****
*****
******
*******

'''

number = int(input("Enter a number: "))

for counter in range(1, number + 1):
    print("*"*counter)



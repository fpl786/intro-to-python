import random

numbers = []

for i in range(10):
    numbers.append(random.randint(1, 50))


guess = int(input("Guess a number: "))


if guess in numbers:
    print("You win!")
else:
    print("You lose!")


print("Winning Numbers: ")
for i in numbers:
    print(i,end = " ")

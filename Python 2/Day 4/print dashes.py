'''

generate a random animal. print a dash for
every letter in the animal

the dashes must be side by side.

You may not use the string replication operator (*)

Eg.
zebra
-----


Modify this to include a function which
the animal name as an argument and prints the dashes

'''

def print_dashes(animal):
    for i in animal:
        print("-", end = " ")


import random

file = open("animals.txt", "r")


animals = file.readlines()

a = random.choice(animals).strip()

print(a)

print_dashes(a)





'''
generate a list of 10 random numbers from 1 to 50
ask the user to guess a number
check if the number is in the list or not


if the number is in the list - print you win
otherwise print - you lose

'''

import random

numbers = []

for i in range(10):
    numbers.append(random.randint(1, 50))


guess = int(input("Guess a number: "))

found = False
for number in numbers:
    if number == guess:
        found = True
        
if found:
    print("You win!")
else:
    print("You lose!")

print("Winning Numbers: ")
for i in numbers:
    print(i,end = " ")

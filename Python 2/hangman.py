'''
Author: Firoz Lakhani
Date Created: August 19, 2021
Filename: hangman.py

Remake of Hangman

'''

import turtle
import random

'''
use the animals.txt file to generate a random animal
'''
file = open("animals.txt", "r")
animals = file.readlines()
animal = random.choice(animals).strip()
file.close()

print(animal)

t = turtle.Turtle()
t.hideturtle()
# draws the structure for the hangman
t.penup()
t.goto(-250,200)


t.pendown()
t.backward(50)
t.right(90)
t.forward(200)

t.right(90)
t.forward(30)
t.backward(60)

t.penup()
t.goto(-250,200)
t.left(90)
t.pendown()
t.forward(20)
t.right(90)


#head
def head():
    t.circle(20)

#body
def body():
    t.left(90)
    t.penup()
    t.forward(40)
    t.pendown()
    t.forward(100)

#right arm
def right_arm():
    t.backward(80)
    t.right(45)
    t.forward(30)
    t.backward(30)

#left arm
def left_arm():
    t.left(90)
    t.forward(30)
    t.backward(30)
    t.right(45)
    t.forward(80)

#right leg
def right_leg():
    t.right(45)
    t.forward(30)
    t.backward(30)

#left leg
def left_leg():
    t.left(90)
    t.forward(30)


'''
create a second turtle object
for every letter in the animal's name
draw a dash on the screen. Each dash should be
50 px long with 10px of space between each dash

The turtle should start drawing at position (0,0)
'''
t2 = turtle.Turtle()
t2.hideturtle()
for i in animal:
    t2.pendown()
    t2.forward(50)
    t2.penup()
    t2.forward(10)


'''
Figure out how to deal with incorrect guesses.
1 time:  draw the head
2 times: body
3 times: right arm
4 times: left arm
5 times: right leg
6 times: left leg
'''

incorrect_guesses = 0
correct_guesses = 0
guesses= []

while incorrect_guesses < 6 and correct_guesses < len(animal):

    guess = turtle.textinput("Hangman", "Guess a letter: ")

    '''
    modify this so that a hangman part is only
    drawn if the letter has not been guessed previously
    '''
    
    if (guess not in animal) and (guess not in guesses):
        incorrect_guesses+=1
        if incorrect_guesses == 1:
            head()
        elif incorrect_guesses == 2:
            body()
        elif incorrect_guesses == 3:
            right_arm()
        elif incorrect_guesses == 4:
            left_arm()
        elif incorrect_guesses == 5:
            right_leg()
        elif incorrect_guesses == 6:
            left_leg()

        t2.penup()
        t2.goto(60*(incorrect_guesses-1) + 25, -50)
        t2.pencolor("red")
        t2.pendown()
        t2.write(guess, False, align = 'center', font = ("Arial", 30))
        t2.penup()

        
    else:
        if guess not in guesses:
            for i in range(len(animal)):
                if guess == animal[i]:
                    correct_guesses += 1
                    t2.pencolor("black")
                    t2.penup()
                    t2.goto(60*i+25, 10)
                    t2.pendown()
                    t2.write(guess, False, align = 'center', font = ("Arial", 30))
                    
    if guess not in guesses:
        guesses.append("guess")
            
                
                
        
t2.penup()
t2.home()
t2.pencolor("Black")

turtle.clearscreen()

turtle.bgcolor("orange")

if incorrect_guesses == 6:
    t2.write("Sorry you lost!\n Correct answer: "+ animal, False, align = 'center', font = ("Arial", 30))

else:
    t2.write("You Win!", False, align = 'center', font = ("Arial", 30))







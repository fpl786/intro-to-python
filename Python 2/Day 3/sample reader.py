'''
for each person listed in the sample.txt file
print the following:

{Name} is {age} years old and lives in {Country}


Your program should run for any file of any size with same details

YOU MUST USE A LOOP FOR THIS
'''

file = open("sample.txt", "r")


info = file.readlines()
'''
for i in range(int(len(info)/3)):
    print(info[3*i].strip()+" is " + info[3*i+1].strip() + " years old and lives in " + info[3*i+2].strip())
'''
for i in range(0, len(info), 3):
    print(info[i].strip()+" is " + info[i+1].strip() + " years old and lives in " + info[i+2].strip())
    


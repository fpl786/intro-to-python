'''
create a guessing game with animals.
generate a random animal from animals.txt
and give the user 5 guesses.

If they get it correct, print "You win" and
exit the program.

Otherwise print try again.

If they run out of guesses print
"You lost! the correct answer is _____"

HINT: use the random.choice function and file.readlines()


'''

import random

file = open("animals.txt", "r")

animals = file.readlines()

animal = random.choice(animals).strip()
print(animal)

# range consists of the numbers 0,1,2,3,4
for i in range(5):
    guess = input("Guess the animal: ")

    if guess.lower() == animal:
        print("Correct!")
        break
    elif i!=4:
        print("Try Again")
    else:
        print("Sorry you lost! The correct answer is "+ animal)


        

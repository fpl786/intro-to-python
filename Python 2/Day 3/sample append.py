'''

write a program that will modify the sample.txt file
to add information for 5 more people

'''

file = open("sample.txt", "a")

lines = "Willis 16 Nigeria Matt 35 Japan Joe 23 U.S.A".split()
print(lines)

for i in lines:
    file.write(i +"\n")


'''
file.write("Willis\n")
file.write("16\n")
file.write("Nigeria\n")

file.write("Matt\n")
file.write("35\n")
file.write("Japan\n")

file.write("Joe\n")
file.write("23\n")
file.write("U.S.A\n")
'''

file.close()

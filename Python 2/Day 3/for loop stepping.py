print("Regular for loop")
#regular for loop

for i in range(4,25):
    print(i)
    

print("For loop with stepping")
for i in range(4,25,2):
    print(i)
    
    
print("counting backwards")
for i in range(24,3,-2):
    print(i)

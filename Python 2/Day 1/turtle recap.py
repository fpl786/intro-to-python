import turtle

t = turtle.Turtle()

t.shape("turtle")
t.pencolor("deep pink")
t.pensize(10)
t.shapesize(2)


t.fillcolor("chartreuse")


t.begin_fill()

for i in range(4):
    t.forward(100)
    t.left(90)
t.end_fill()

t.penup()
t.backward(100)
t.pendown()


t.begin_fill()
t.circle(30)
t.end_fill()

t.setheading(360)

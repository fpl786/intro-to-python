'''
Ask the user to type in a word.
If the word is a palindrome, print "Your word is a palindrome"
otherwise print "Your word is not a palindrome"

Eg. 1
Type a word
racecar
Your word is a palindrome

Eg. 2
Type a word
hello
Your word is not a palindrome


'''


word = input()

if word == word[::-1]:
    print("Your word is a palindrome")
else:
    print("Your word is not a palindrome")

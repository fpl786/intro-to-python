'''
write a program that takes 2 numbers as input
the first number will be the base and the second will
be the exponent. Calculate the power and print it.

'''

print("Enter the base: ")

base = int(input())

print("Enter the exponent: ")

exponent = int(input())


print("The answer is", base**exponent)

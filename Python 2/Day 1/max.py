'''
write a program that calculates the maximum value of any list of numbers

Eg.
numbers = [2,45,6,2,146,6,1,6]
Max: 146

'''
import random

numbers = []


for i in range(10):
    numbers.append(random.randint(-250, 250))

print(numbers)

maximum = numbers[0]


for i in numbers:
    if i > maximum:
        maximum = i
        
print("Max: ", maximum)

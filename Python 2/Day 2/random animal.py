'''
using the animals.txt file that you downloaded. Generate a
random animal from the file and print it to the screen

HINT: Use random.choice() and file.readlines()
'''
import random


file = open("animals.txt", "r")

animals = file.readlines()

animal = random.choice(animals).strip()

print(animal)

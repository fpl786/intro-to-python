'''
Using the hello.txt file we created write a program that copies
all of the contents of the file into a new file called hello-new.txt

'''

file = open("hello.txt", "r")

lines = file.read()

file.close()


new_file = open("hello-new.txt", "w")

new_file.write(lines)

new_file.close()

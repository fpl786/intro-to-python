'''
file.read() reads all the contents of the file and returns it as a string
file.read(n) reads n bytes from the file and returns it as a string
file.readline() reads the next line from a file and returns it as a string
file.readlines() returns a list of all remaining lines from a file

'''


file = open("hello.txt", "r")



lines = file.read()



print(lines)

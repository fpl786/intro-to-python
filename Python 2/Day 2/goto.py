'''
write a program using turtle that allows the user
to click anywhere on the screen. Move the turtle to the exact
location where the mouse was clicked

HINT: Use the goto method in turtle

'''
import turtle

t = turtle.Turtle()
t.speed(5)
def move_to(x,y):
    t.goto(x,y)


turtle.listen()

turtle.onscreenclick(move_to, 1)

turtle.mainloop()

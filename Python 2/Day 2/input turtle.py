'''

take in 4 inputs

fillcolor of  square
pencolor of square

fillcolor of circle
pencolor of circle

draw a square and circle based on the inputs given

'''
import turtle

penSquare = input("Enter pen colour of the square: ")
fillSquare = input("Enter fill colour of the square: ")
penCircle = input("Enter pen colour of the circle: ")
fillCircle = input("Enter fill colour of the circle: ")

t = turtle.Turtle()
t.pensize(3)
t.pencolor(penSquare)
t.fillcolor(fillSquare)


t.begin_fill()
for i in range(4):
    t.forward(100)
    t.right(90)
t.end_fill()

t.pencolor(penCircle)
t.fillcolor(fillCircle)


t.penup()
t.backward(200)
t.pendown()

t.begin_fill()
t.circle(50)
t.end_fill()
